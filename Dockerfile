FROM ubuntu

RUN sed -i s@/archive.ubuntu.com/@/mirrors.ucloud.cn/@g /etc/apt/sources.list   \
    &&  apt-get update  \
    # 需要解决时区问题
    &&  export DEBIAN_FRONTEND=noninteractive   \
    &&  apt-get upgrade -y  \
    &&  apt-get install -y  \
        build-essential cmake autoconf libtool pkg-config libssl-dev    \
        git \
        libpq5 libpq-dev postgresql-server-dev-all  \
    &&  ln -fs /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \
    &&  dpkg-reconfigure -f noninteractive tzdata   \
    &&  apt-get autoremove  \
    &&  apt-get clean

RUN git clone https://gitee.com/mirrors/libpqxx.git    \
    &&  cd libpqxx  \
    &&  cmake . -DSKIP_BUILD_TEST=on -DINSTALL_TEST=off \
    &&  make -j4    \
    &&  make install    \
    &&  cd ..   \
    &&  rm -rf libpqxx

COPY grpc/ grpc/

RUN cd grpc \
    # 来自https://gitee.com/mirrors/grpc/blob/master/test/distrib/cpp/run_distrib_test_cmake.sh
    # absl
    &&  cd third_party  &&  export THIRD_PARTY_DIR=$(pwd)   \
    &&  mkdir -p ${THIRD_PARTY_DIR}/abseil-cpp/cmake/build  &&  cd ${THIRD_PARTY_DIR}/abseil-cpp/cmake/build    \
    &&  cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE ../..   \
    &&  make -j4 install    \
    # c-ares
    &&  mkdir -p ${THIRD_PARTY_DIR}/cares/cares/cmake/build &&  cd ${THIRD_PARTY_DIR}/cares/cares/cmake/build   \
    &&  cmake -DCMAKE_BUILD_TYPE=Release ../..  \
    &&  make -j4 install    \
    # protobuf
    &&  mkdir -p ${THIRD_PARTY_DIR}/protobuf/cmake/build    &&  cd ${THIRD_PARTY_DIR}/protobuf/cmake/build  \
    &&  cmake -Dprotobuf_BUILD_TESTS=OFF -DCMAKE_BUILD_TYPE=Release ..  \
    &&  make -j4 install    \
    # re2
    &&  mkdir -p ${THIRD_PARTY_DIR}/re2/cmake/build &&  cd ${THIRD_PARTY_DIR}/re2/cmake/build   \
    &&  cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_POSITION_INDEPENDENT_CODE=TRUE ../..   \
    &&  make -j4 install    \
    # zlib
    &&  mkdir -p ${THIRD_PARTY_DIR}/zlib/cmake/build    &&  cd ${THIRD_PARTY_DIR}/zlib/cmake/build  \
    &&  cmake -DCMAKE_BUILD_TYPE=Release ../..  \
    &&  make -j4 install    \
    # gRPC
    &&  mkdir -p ${THIRD_PARTY_DIR}/../cmake/build  &&  cd ${THIRD_PARTY_DIR}/../cmake/build    \
    &&  cmake ../.. \
        -DCMAKE_BUILD_TYPE=Release  \
        -DgRPC_INSTALL=ON   \
        -DgRPC_BUILD_TESTS=OFF  \
        -DgRPC_CARES_PROVIDER=package   \
        -DgRPC_ABSL_PROVIDER=package    \
        -DgRPC_PROTOBUF_PROVIDER=package    \
        -DgRPC_RE2_PROVIDER=package \
        -DgRPC_SSL_PROVIDER=package \
        -DgRPC_ZLIB_PROVIDER=package    \
    &&  make -j4 install    &&  cd ../../.. \
    &&  rm -rf grpc

RUN git clone https://gitee.com/mirrors/glog.git    \
    && cd glog  \
    && mkdir build && cd build  \
    && cmake .. && cmake --build . --target install \
    && cd ../.. && rm -rf glog

COPY . /routing-service
RUN cd /routing-service  \
    && cmake . && make -j4  \
    && mv server/routing_server server/config.json client/routing_client client/task_points.txt /   \    
    && cd .. && rm -rf /routing-service 

# 启动
CMD ["./routing_server"]
