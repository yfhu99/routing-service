#include "DBManager.h"

#include <iomanip>

DBManager& DBManager::getInstance() {
  static DBManager instance;
  return instance;
}

int DBManager::findReferenceRoad(
    const std::string& map, const std::pair<double, double>& start_pos,
    const std::pair<double, double>& end_pos,
    std::vector<pqxx::result, std::allocator<pqxx::result>>& results_vec) {
  auto pConn = pool.getConnection(map);
  if(pConn==nullptr){
    LOG(ERROR)<<"database connection disconnect";
    return -1;
  }
  pConn->transaction();
  pConn->qeury("drop table if exists shortpath");
  std::ostringstream sql;
  //默认小数点后7位
  sql.setf(std::ios::fixed);
  sql.precision(7);
  sql << "select pgr_fromatob('topo_map', " << start_pos.first << ", "
      << start_pos.second << ", " << end_pos.first << ", " << end_pos.second
      << ") as pgr_fromatontob into temp shortpath";
  pConn->qeury(sql.str());
  //这里可能找不到最短路径
  // if (!pConn->getStatus()) return -1;
  pConn->qeury("select line2points('shortpath')");
  pqxx::result points = pConn->qeury("select * from result_path_points");
  pqxx::result cost = pConn->qeury("select calculate_cost('topo_map')");
  if (!pConn->getStatus()) return -1;
  //关闭transcation
  pConn->commit();
  results_vec.push_back(points);
  //按照要求需要转换成int
  return static_cast<int>(cost[0][0].as<double>());
}

int DBManager::findReferenceRoadBlocked(
    const std::string& map, const std::pair<double, double>& blocked_point,
    const std::pair<double, double>& end_pos,
    std::vector<pqxx::result, std::allocator<pqxx::result>>& results_vec) {
  auto pConn = pool.getConnection(map);
  if(pConn==nullptr){
    LOG(ERROR)<<"database connection disconnect";
    return -1;
  }
  pConn->transaction();
  std::stringstream sql;
  sql.setf(std::ios::fixed);
  sql.precision(7);
  sql << "select pgr_updateTopoMap('topo_map', " << blocked_point.first
      << ", " << blocked_point.second << ")";
  pqxx::result new_point = pConn->qeury(sql.str());
  if (!pConn->getStatus()) return -1;
  double new_start_lon = new_point[0][0].as<double>(),
         new_start_lat = new_point[1][0].as<double>();
  sql.str("");
  sql << "select pgr_fromatob('topo_map', " << new_start_lon << ", "
      << new_start_lat << ", " << end_pos.first << ", " << end_pos.second
      << ") as pgr_fromatontob into temp shortpath";
  pConn->qeury(sql.str());
  //这里可能找不到最短路径
  // if (!pConn->getStatus()) return -1;
  pConn->qeury("select * from line2points('shortpath')");
  pqxx::result points = pConn->qeury("select * from result_path_points");
  pqxx::result cost = pConn->qeury("select calculate_cost('topo_map')");
  if (!pConn->getStatus()) return -1;
  //关闭transcation
  pConn->commit();
  results_vec.push_back(points);
  //按照要求需要转换成int
  return static_cast<int>(cost[0][0].as<double>());
}

pqxx::result DBManager::getClosestSemanticPoint(
    const std::string& map, const std::pair<double, double>& task_point) {
  pqxx::result point;
  auto pConn = pool.getConnection(map);
  if(pConn==nullptr){
    LOG(ERROR)<<"database connection disconnect";
    point.clear();
    return point;
  }
  std::ostringstream sql;
  sql.setf(std::ios::fixed);
  sql.precision(7);
  sql << "select st_x(geom), st_y(geom), utmx, utmy, heading from "
         "all_points_test order by geom <-> 'SRID=4326;POINT("
      << task_point.first << " " << task_point.second << ")'::geometry limit 1";
  pConn->transaction();
  point = pConn->qeury(sql.str());
  if (!pConn->getStatus()) {
    point.clear();
    return point;
  }
  pConn->commit();
  return point;
}