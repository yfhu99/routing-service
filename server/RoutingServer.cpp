#include <glog/logging.h>
#include <grpcpp/ext/proto_server_reflection_plugin.h>
#include <grpcpp/grpcpp.h>
#include <grpcpp/health_check_service_interface.h>

#include <iostream>
#include <memory>
#include <pqxx/pqxx>
#include <sstream>
#include <string>
#include <thread>
#include <unordered_map>

#include "Interceptor/LoggingInterceptor.h"
#include "MapService/MapServiceImpl.h"
#include "RoutingService/RoutingServiceImpl.h"
#include "routing_service.grpc.pb.h"
#include "utils/Configuration.h"
#include "utils/Logger.h"

using grpc::Server;
using grpc::ServerBuilder;

void RunServer() {
  std::string server_address("0.0.0.0:50051");
  std::string configuration_path = "config.json";
  //读取配置信息
  ConfigMananger::getInstance().init(configuration_path);
  RoutingServiceImpl routingService;
  MapServiceImpl mapService;

  grpc::EnableDefaultHealthCheckService(true);
  grpc::reflection::InitProtoReflectionServerBuilderPlugin();
  ServerBuilder builder;
  // Listen on the given address without any authentication mechanism.
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which we'll communicate with
  // clients. In this case it corresponds to an *synchronous* service.
  builder.RegisterService(&routingService);
  builder.RegisterService(&mapService);
  //添加Interceptor
  std::vector<
      std::unique_ptr<grpc::experimental::ServerInterceptorFactoryInterface>>
      creators;
  creators.push_back(
      std::unique_ptr<grpc::experimental::ServerInterceptorFactoryInterface>(
          new LoggingInterceptorFactory()));
  builder.experimental().SetInterceptorCreators(std::move(creators));
  // Finally assemble the server.
  std::unique_ptr<Server> server(builder.BuildAndStart());
  LOG(INFO) << "Server listening on " << server_address;

  // Wait for the server to shutdown. Note that some other thread must be
  // responsible for shutting down the server for this call to ever return.
  server->Wait();
}

int main(int argc, char** argv) {
  //打开glog，默认情况下写到/tmp/\<program name\>.\<hostname\>.\<user
  // name\>.log.\<severity level\>.\<date\>.\<time\>.\<pid\>中
  //同时ERROR和FATAL的错误会输出到std::err
  google::InitGoogleLogging(argv[0]);
  //改变log位置
  FLAGS_log_dir = ".";
  //内部错误定向到glog
  gpr_set_log_function(grpc_error_to_glog);
  RunServer();
  return 0;
}
