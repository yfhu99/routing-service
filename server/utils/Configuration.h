#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <glog/logging.h>

#include <fstream>
#include <mutex>
#include <string>

#include "3rdParty/json.hpp"
using nlohmann::json;
class ConfigMananger {
 private:
  ConfigMananger() = default;
  ~ConfigMananger() = default;
  ConfigMananger(const ConfigMananger&) = delete;
  ConfigMananger& operator=(const ConfigMananger&) = delete;

  std::mutex init_mutex;
  json config;

 public:
  static ConfigMananger& getInstance() {
    static ConfigMananger instance;
    return instance;
  }

  //读取文件
  void init(const std::string& path) {
    std::lock_guard<std::mutex> lk(init_mutex);
    if (!config.empty()) LOG(FATAL) << "configuration already read";
    std::ifstream infile(path);
    if (!infile) LOG(FATAL) << "no such configuration file";
    infile >> config;
  }

  //获取某种类型的配置数据，注意只有at方法才会有异常
  template <typename T>
  T getConfigByNameAs(const std::string& name) {
    try {
      return config.at(name).get<T>();
    } catch (const std::exception& e) {
      LOG(FATAL) << e.what();
    }
  }
};

#endif