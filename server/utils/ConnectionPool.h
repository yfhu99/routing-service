#ifndef CONNECTION_POOL_H
#define CONNECTION_POOL_H

#include <glog/logging.h>

#include <chrono>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <pqxx/pqxx>
#include <queue>
#include <thread>

#include "ConcurrentHashMap.h"

/*
封装连接信息
*/

struct Connection {
  std::chrono::time_point<std::chrono::steady_clock>
      aliveTime;  //从何时起进入队列

  //刷新进入队列时间
  void refreshAliveTime() { aliveTime = std::chrono::steady_clock::now(); }
  //获取连接从进入队列到现在多长时间
  size_t getAliveTime() const {
    return std::chrono::duration_cast<std::chrono::milliseconds>(
               std::chrono::steady_clock::now() - aliveTime)
        .count();
  }
  Connection(const std::string &connect_url)
      : conn(connect_url), p_work(nullptr), done(true) {}
  bool getStatus() { return done; }
  void transaction() {
    //启动一个新work
    done = true;
    p_work.reset(new pqxx::work(conn));
  }
  pqxx::result qeury(const std::string &sql) {
    pqxx::result r;
    try {
      LOG(INFO) << "execute SQL: " << sql;
      r = p_work->exec(sql);
    } catch (pqxx::sql_error const &e) {
      // sql错误应当被catch
      LOG(ERROR) << "SQL error: " << e.what();
      done = false;
    }
    return std::move(r);
  }
  void commit() {
    if (done) p_work->commit();
  }
  bool is_open(){
    return conn.is_open();
  }
 private:
  pqxx::connection conn;
  std::unique_ptr<pqxx::work> p_work;
  bool done;  //一个transaction是否正常执行
};

//每个数据库一个结构
class ConnectionPoolInternal {
 private:
  std::string connect_url;         //连接url
  size_t initSize = 10;            //初始池内连接数量
  size_t maxSize = 85;           //最大连接数量
  size_t maxIdleTime = 60;         //空闲连接等待回收时间，单位秒
  size_t connectionTimeout = 100;  //获取连接超时时间，单位毫秒

  //单生产者单消费者的队列，基于互斥量和条件变量实现
  std::queue<Connection *> connectionQueue;
  std::mutex queueMutex;
  std::condition_variable cv;
  size_t connectionCnt = 0;  //记录当前连接总数

  void produceConnection();
  void scanConnection();

  //拷贝和拷贝赋值必须删除
  ConnectionPoolInternal(const ConnectionPoolInternal &) = delete;
  ConnectionPoolInternal &operator=(const ConnectionPoolInternal &) = delete;

 public:
  ConnectionPoolInternal(const std::string &);
  std::shared_ptr<Connection> getConnection();
};

class ConnectionPool {
 private:
  //从配置中加载
  std::string user;      //用户名
  std::string password;  //密码
  std::string host;      // IP
  std::string port;      //端口

  ConcurrentHashMap<std::string, std::shared_ptr<ConnectionPoolInternal>> list;

  ConnectionPool(const ConnectionPool &) = delete;
  ConnectionPool &operator=(const ConnectionPool &) = delete;

 public:
  ConnectionPool();
  // ConnectionPool &getInstance() {
  //   static ConnectionPool instance;
  //   return instance;
  // }
  std::shared_ptr<Connection> getConnection(const std::string &);
};
#endif