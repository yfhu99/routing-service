#ifndef ROUTING_SERVICE_IMPL_H
#define ROUTING_SERVICE_IMPL_H

#include "../../protos/routing_service.grpc.pb.h"

using grpc::ServerContext;
using grpc::Status;

using routing_service::RefRoad;
using routing_service::RoutingService;
using routing_service::TaskPoints;

//实现service方法
class RoutingServiceImpl final : public RoutingService::Service {
  Status FindReferenceRoad(ServerContext*, const TaskPoints*,
                           RefRoad*) override;
};
#endif