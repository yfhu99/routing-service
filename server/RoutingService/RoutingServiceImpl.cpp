#include "RoutingServiceImpl.h"

#include <pqxx/pqxx>

#include "../DBManager/DBManager.h"

using grpc::StatusCode;

Status RoutingServiceImpl::FindReferenceRoad(ServerContext* context,
                                             const TaskPoints* request,
                                             RefRoad* response) {
  std::string map = request->map();
  bool is_blocked = request->blocked();
  std::vector<pqxx::result, std::allocator<pqxx::result>>
      results_vec;  //查询记录
  auto& dbm = DBManager::getInstance();
  int sum_costs = 0;
  int points_size = request->task_point_size();
  for (int start_idx = 0; start_idx < points_size - 1; ++start_idx) {
    int cost;
    std::pair<double, double> start_pos = {
        request->task_point(start_idx).lon(),
        request->task_point(start_idx).lat()};
    std::pair<double, double> end_pos = {
        request->task_point(start_idx + 1).lon(),
        request->task_point(start_idx + 1).lat()};
    if (is_blocked == false) {
      cost = dbm.findReferenceRoad(map, start_pos, end_pos, results_vec);
    } else {
      is_blocked = false;  //第一段路径执行调头规划后, 后面的路段规划不再执行
      cost = dbm.findReferenceRoadBlocked(
          map, {request->task_point(0).lon(), request->task_point(0).lat()},
          end_pos, results_vec);
    }
    if (cost == -1)
      return Status(StatusCode::INVALID_ARGUMENT, "can't find refernece road");
    sum_costs += cost;
  }
  //原来是返回，这里是写入response
  response->set_time_cost(sum_costs);

  //原来是写入文件，这里变为写入response
  for (const auto& result : results_vec) {
    for (auto iter = result.begin(); iter != result.end(); iter++) {
      //对每一行
      auto point = response->add_point();
      point->set_id(iter->at(0).as<int>());
      point->set_lon(iter->at(1).as<double>());
      point->set_lat(iter->at(2).as<double>());
      point->set_utmx(iter->at(3).as<double>());
      point->set_utmy(iter->at(4).as<double>());
      point->set_heading(iter->at(5).as<double>());
      point->set_curv(iter->at(6).as<double>());
      point->set_mode(iter->at(7).as<int>());
      point->set_speed_mode(iter->at(8).as<int>());
      point->set_event_mode(iter->at(9).as<int>());
      point->set_opposite_side_mode(iter->at(10).as<int>());
      point->set_lane_num(iter->at(11).as<int>());
      point->set_lane_seq(iter->at(12).as<int>());
      point->set_lane_width(iter->at(13).as<double>());
    }
  }
  return Status::OK;
}
